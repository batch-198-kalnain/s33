// Express package was imported as express.
const express = require("express");

// Invoked express package to create a server/api and saved it in varibale which we can refer to later to create routes.
const app = express();

  //////////////////////
 //  express.json()  //
//////////////////////

// express.json() - is a method from express that allow us to handle the stream of data from our client and receive the data and automatically parse the incoming JSON from the request.

  ///////////////////
 //   app.use()   // 
/////////////////// 

// app.use() - is a method used to run another function or method for our expressjs api. It is used to run middlewares (functions that add features to our application)
app.use(express.json());

// variable for port assignment
const port = 4000;

// Mock collection for courses
let courses = [
	{
		name:"Python 101",
		description:"Learn Python",
		price:25000
	}, 
	{
		name:"ReactJS 101",
		description:"Learn React",
		price:35000
	},
	{
		name:"ExpressJS 101",
		description:"Learn ExpressJS",
		price:28000
	}
];

let users = [
	{
		email:"marybell_knight",
		password:"merrymarybell"
	},
	{
		email:"janedoePriest",
		password:"jobPriest100"
	},
	{
		email:"kimTofu",
		password:"dubuTofu"
	}
];

// used the listen() - method of express to assign a port to our server and send a message

  /////////////////////////////////////
 //   creating a route in Express:  //
/////////////////////////////////////

// access express to have access to its route methods.
/*
	app.method('/endpoint',(request,response)=>{

		response.send();

		send() - is a method similar to end() that it sends the data/message and ends the response. It also automatically creates and adds the headers.
	})
*/

app.get('/',(req,res)=>{
	res.send("Hello from our first ExpressJS route!");
})

app.post('/',(req,res)=>{
	res.send("Hello from our first ExpressJS Post Method Route!");
})

app.put('/',(req,res)=>{
	res.send("Hello from a PUT Method Route!");
})

app.delete('/',(req,res)=>{
	res.send("Hello from a DELETE Method Route!");
})

app.get('/courses',(req,res)=>{
	res.send(courses);
})

app.post('/courses',(req,res)=>{
	// console.log(req.body);
	courses.push(req.body);
	res.send(courses);
})


  ///////////////////////////
 //   ACTIVITY SOLUTION   //
///////////////////////////
app.get('/users',(req,res)=>{
	res.send(users);
})

app.post('/users',(req,res)=>{
	users.push(req.body);
	res.send(users);
})
  
  ///////////////////////////////
 //  Stretch Goal 1 Solution  //
///////////////////////////////
app.delete('/users',(req,res)=>{
	users.pop();
	res.send("A user has been deleted successfully");
})

  ///////////////////////////////
 //  Stretch Goal 2 Solution  //
///////////////////////////////
app.patch('/courses',(req,res)=>{
	let reqCourse = courses[req.body.id];
	// console.log(reqCourse);
	
	reqCourse.price = req.body.price;
	// console.log(reqCourse);

	res.send(courses);
})

app.listen(port,()=> console.log("Express Api running at port 4000"))